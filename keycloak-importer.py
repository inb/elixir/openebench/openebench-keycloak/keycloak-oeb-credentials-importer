#!/usr/bin/env python3

import sys
import json
import urllib.request
import urllib.parse

OPENEBENCH_URL = "https://dev-openebench.bsc.es/api/scientific/access"
KEYCLOAK_TOKEN_ENDPOINT = "https://inb.bsc.es/auth/realms/openebench/protocol/openid-connect/token"
KEYCLOAK_OPENEBENCH_REALM = "https://inb.bsc.es/auth/admin/realms/openebench/"

def main():

  if (len(sys.argv) > 1):
    username = sys.argv[1]
  else:
    print('username:', end = ' ')
    username = str(input())

  if (len(sys.argv) > 2):
    password = sys.argv[2]
  else:
    print('password:', end = ' ')
    password = str(input())

  token = getAdminToken(username, password)
  headers = {'Authorization' : 'Bearer ' + token}

  updateCommunities(headers)
  updatePrivileges(headers)


# Update OpenEBench communities groups in the Keycloak server
def updateCommunities(headers):

  communities = getOpenebenchCommunities()

  root = getKeycloakCommunities(headers)
  groups = root['subGroups']

  # update existing communities
  for group in groups[:]:
    attributes = group['attributes']
    roles = attributes.get('oeb:roles')
    if (roles != None):
      for role in roles:
        if (role.startswith('owner:')):
          for community in communities[:]:
            if (community['_id'] == role[6:]):
              #attributes['acronym'] = [community['acronym']]
              updateCommunityGroup(group, headers)
              communities.remove(community)
              groups.remove(group)
              break
    
  # insert new communities
  for community in communities:
    group = {}
    group['name'] = community['name']
    #group['attributes'] = {'community_id' : [community['_id']], 'acronym' : [community['acronym']]}
    group['attributes'] = {'oeb:roles' : ['owner:' + community['_id']], 'acronym' : [community['acronym']]}
    addCommunityGroup(root, group, headers)

  #remove old communities
  for group in groups:
    deleteCommunityGroup(group, headers)

# Update OpenEBench users communities on the Keycloak server
def updatePrivileges(headers):

  root = getKeycloakCommunities(headers)
  groups = root['subGroups']

  contacts = getOpenebenchContacts()
  users = getKeycloakUsers(headers)
  privileges = getOpenebenchPrivileges()

  for contact in contacts:
    emails = contact.get('email')
    if (emails != None):
      for user in users:
        for email in emails:
          if (email == user['email']):
            for privilege in privileges:
              if (privilege['_id'] == contact['_id']):
                roles = privilege.get('oeb:roles')
                if (roles != None):
                  for role in roles:
                    community_id = role.get('community_id')
                    if (community_id != None):
                      addContactToCommunity(groups, user, community_id, headers)
                    else:
                      r = role.get('role')
                      challenge_id = role.get('challenge_id')
                      if (r != None and challenge_id != None):
                        addContactToChallenge(user, r, challenge_id, headers)
 
# Assigns the user to the community group (owner)
def addContactToCommunity(groups, user, community_id, headers):

  print('adding user ' + user['username'] + ' to the community ' + community_id)
  
  for group in groups:
    attributes = group['attributes']
    roles = attributes.get('oeb:roles')
    if (roles != None):
      for role in roles:
        if (role.startswith('owner:') and community_id == role[6:]):
          addKeycloakUserToGroup(user, group, headers)

# Assigns the user role to the challenge
def addContactToChallenge(user, role, challenge_id, headers):

  print('adding user ' + role + ':' + user['username'] + ' to the challenge ' + challenge_id)

  r = role + ':' + challenge_id 
  attributes = user['attributes']
  rz = attributes.get('oeb:roles')

  if (rz == None):
    attributes['oeb:roles'] = [r]
    updateKeycloakUser(user, headers)
  elif (not r in rz):
    rz.append(r)
    updateKeycloakUser(user, headers)


# Update the openebench community ('group')
def updateCommunityGroup(group, headers):
  
  print('updating community ' + group['name'])   

  KC_OEB_SUBGROUP = KEYCLOAK_OPENEBENCH_REALM + 'groups/' + group['id']

  group_req = urllib.request.Request(KC_OEB_SUBGROUP, data=json.dumps(group).encode("utf-8"), headers=headers, method='PUT')
  group_req.add_header("Content-type", "application/json; charset=UTF-8")

  group_res = urllib.request.urlopen(group_req);

  if(group_res.getcode() >= 400):
    print("error updating openebench community", group_req)


# Insert the openebench community ('group') into the 'Community' group ('root')
def addCommunityGroup(root, group, headers):

  print('inserting community ' + group['name'])

  KC_OEB_SUBGROUP = KEYCLOAK_OPENEBENCH_REALM + 'groups/' + root['id'] + '/children'

  group_req = urllib.request.Request(KC_OEB_SUBGROUP, data=json.dumps(group).encode("utf-8"), headers=headers)
  group_req.add_header("Content-type", "application/json; charset=UTF-8")

  group_res = urllib.request.urlopen(group_req);

  if(group_res.getcode() >= 400):
    print("error adding openebench community", group_req)

# Remove the openebench community ('group') from the 'Community' root group
def deleteCommunityGroup(group, headers):

  print('removing community ' + group['name'])

  KC_OEB_SUBGROUP = KEYCLOAK_OPENEBENCH_REALM + 'groups/' + group['id']

  group_req = urllib.request.Request(KC_OEB_SUBGROUP, method='DELETE')
  group_res = urllib.request.urlopen(group_req);

  if(group_res.getcode() >= 400):
    print("error removing openebench community", group_req)

# Get the 'Community' group with all its subgroups (openebench communities)
def getKeycloakCommunities(headers):
    
  KC_OEB_GROUPS = KEYCLOAK_OPENEBENCH_REALM + 'groups?briefRepresentation=false'

  groups_req = urllib.request.Request(KC_OEB_GROUPS, headers=headers)
  groups_res = urllib.request.urlopen(groups_req);
  if(groups_res.getcode() >= 400):
    print("error obtaining openebench user groups", groups_req)

  data = groups_res.read()
  groups = json.loads(data)

  for group in groups:
    if (group['name'] == 'Community'):
      return group

  ## add Community group  
  KC_OEB_GROUPS = KEYCLOAK_OPENEBENCH_REALM + 'groups'

  root = {'name' : 'Community'}

  group_req = urllib.request.Request(KC_OEB_GROUPS, data=json.dumps(root).encode("utf-8"), headers=headers)
  group_req.add_header("Content-type", "application/json; charset=UTF-8")

  group_res = urllib.request.urlopen(group_req);

  if(group_res.getcode() >= 400):
    print("error adding Keycloak 'Community' group ", group_req)

# Get Keycloak users
def getKeycloakUsers(headers):
    
  KC_OEB_USERS = KEYCLOAK_OPENEBENCH_REALM + 'users?briefRepresentation=false'

  users_req = urllib.request.Request(KC_OEB_USERS, headers=headers)
  users_res = urllib.request.urlopen(users_req);
  if(users_res.getcode() >= 400):
    print("error obtaining openebench users", users_req)

  data = users_res.read()

  return json.loads(data)

# Update Keycloak user
def updateKeycloakUser(user, headers):
    
  print('updating user ' + user['username'])

  KC_OEB_USER = KEYCLOAK_OPENEBENCH_REALM + 'users/' + user['id']

  user_req = urllib.request.Request(KC_OEB_USER, data=json.dumps(user).encode("utf-8"), headers=headers, method='PUT')
  user_req.add_header("Content-type", "application/json; charset=UTF-8")

  user_res = urllib.request.urlopen(user_req);

  if(user_res.getcode() >= 400):
    print("error updating openebench user", user_req)

# Inserts user to the group of users
def addKeycloakUserToGroup(user, group, headers):

  print('putting user ' + user['username'] + ' to group ' + group['id'])

  KC_OEB_GROUP = KEYCLOAK_OPENEBENCH_REALM + 'users/' + user['id'] + '/groups/' + group['id']

  user_req = urllib.request.Request(KC_OEB_GROUP, data=json.dumps(user).encode("utf-8"), headers=headers, method='PUT')
  user_req.add_header("Content-type", "application/json; charset=UTF-8")

  user_res = urllib.request.urlopen(user_req);

  if(user_res.getcode() >= 400):
    print("error assigning the user to a group", user_req)


# Get OpenEBench communities via the REST API
def getOpenebenchCommunities():

  res = urllib.request.urlopen(OPENEBENCH_URL + '/Community');
  if(res.getcode() < 300):
    data = res.read()
    return json.loads(data)

  print("error reading communities", req) 

# Get OpenEBench benchmarking events via the REST API
def getOpenebenchBenchmarkingEvents():

  res = urllib.request.urlopen(OPENEBENCH_URL + '/BenchmarkingEvent');
  if(res.getcode() < 300):
    data = res.read()
    return json.loads(data)

  print("error reading benchmarking events", req) 

# Get OpenEBench contacts via the REST API
def getOpenebenchContacts():

  res = urllib.request.urlopen(OPENEBENCH_URL + '/Contact');
  if(res.getcode() < 300):
    data = res.read()
    return json.loads(data)

  print("error reading contacts", req) 

# Get OpenEBench contacts via the REST API
def getOpenebenchPrivileges():

  res = urllib.request.urlopen(OPENEBENCH_URL + '/Privilege');
  if(res.getcode() < 300):
    data = res.read()
    return json.loads(data)

  print("error reading privileges", req) 

# Get OIDC access token with provided user and password
def getAdminToken(user, password):

  params = {'username' : user, 'password' : password, 'grant_type' : 'password', 'client_id' : 'admin-cli'}
  token_req = urllib.request.Request(KEYCLOAK_TOKEN_ENDPOINT, urllib.parse.urlencode(params).encode('utf-8'));
  token_res = urllib.request.urlopen(token_req)

  if(token_res.getcode() < 300):  
    data = token_res.read()
    jwt = json.loads(data)
    return jwt['access_token']

  print("can't get administration token", req)

if __name__ == "__main__":
    main()